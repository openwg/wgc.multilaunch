//
// Includes
//

// stdlib
#include <vector>

// winapi
#include <Windows.h>
#include <wchar.h>
#include <process.h>
#include <TlHelp32.h>

// wotmutex
#include "wotmutex.h"



//
// Defines
//

#define NT_SUCCESS(x) ((x) >= 0)
#define STATUS_INFO_LENGTH_MISMATCH 0xc0000004

#define SystemHandleInformation 16
#define ObjectBasicInformation 0
#define ObjectNameInformation 1
#define ObjectTypeInformation 2



//
// Typedefs
//

typedef NTSTATUS(NTAPI *_NtQuerySystemInformation)(
	ULONG SystemInformationClass,
	PVOID SystemInformation,
	ULONG SystemInformationLength,
	PULONG ReturnLength
	);

typedef NTSTATUS(NTAPI *_NtDuplicateObject)(
	HANDLE SourceProcessHandle,
	HANDLE SourceHandle,
	HANDLE TargetProcessHandle,
	PHANDLE TargetHandle,
	ACCESS_MASK DesiredAccess,
	ULONG Attributes,
	ULONG Options
	);

typedef NTSTATUS(NTAPI *_NtQueryObject)(
	HANDLE ObjectHandle,
	ULONG ObjectInformationClass,
	PVOID ObjectInformation,
	ULONG ObjectInformationLength,
	PULONG ReturnLength
	);

typedef struct _UNICODE_STRING
{
	USHORT Length;
	USHORT MaximumLength;
	PWSTR Buffer;
} UNICODE_STRING, *PUNICODE_STRING;

typedef struct _SYSTEM_HANDLE
{
	ULONG ProcessId;
	BYTE ObjectTypeNumber;
	BYTE Flags;
	USHORT Handle;
	PVOID Object;
	ACCESS_MASK GrantedAccess;
} SYSTEM_HANDLE, *PSYSTEM_HANDLE;

typedef struct _SYSTEM_HANDLE_INFORMATION
{
	ULONG HandleCount;
	SYSTEM_HANDLE Handles[1];
} SYSTEM_HANDLE_INFORMATION, *PSYSTEM_HANDLE_INFORMATION;

typedef enum _POOL_TYPE
{
	NonPagedPool,
	PagedPool,
	NonPagedPoolMustSucceed,
	DontUseThisType,
	NonPagedPoolCacheAligned,
	PagedPoolCacheAligned,
	NonPagedPoolCacheAlignedMustS
} POOL_TYPE, *PPOOL_TYPE;

typedef struct _OBJECT_TYPE_INFORMATION
{
	UNICODE_STRING Name;
	ULONG TotalNumberOfObjects;
	ULONG TotalNumberOfHandles;
	ULONG TotalPagedPoolUsage;
	ULONG TotalNonPagedPoolUsage;
	ULONG TotalNamePoolUsage;
	ULONG TotalHandleTableUsage;
	ULONG HighWaterNumberOfObjects;
	ULONG HighWaterNumberOfHandles;
	ULONG HighWaterPagedPoolUsage;
	ULONG HighWaterNonPagedPoolUsage;
	ULONG HighWaterNamePoolUsage;
	ULONG HighWaterHandleTableUsage;
	ULONG InvalidAttributes;
	GENERIC_MAPPING GenericMapping;
	ULONG ValidAccess;
	BOOLEAN SecurityRequired;
	BOOLEAN MaintainHandleCount;
	USHORT MaintainTypeList;
	POOL_TYPE PoolType;
	ULONG PagedPoolUsage;
	ULONG NonPagedPoolUsage;
} OBJECT_TYPE_INFORMATION, *POBJECT_TYPE_INFORMATION;



//
// Helpers
//

PVOID GetLibraryProcAddress(PSTR LibraryName, PSTR ProcName)
{
    HMODULE hModule = GetModuleHandleA(LibraryName);
    if (hModule == nullptr) {
        return nullptr;
    }

	return GetProcAddress(hModule, ProcName);
}

std::vector<HANDLE> GetProcessByName(PCSTR name)
{
    std::vector<HANDLE> result;

    // Create toolhelp snapshot.
    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    PROCESSENTRY32 process;
    ZeroMemory(&process, sizeof(process));
    process.dwSize = sizeof(process);

    // Walkthrough all processes.
    if (Process32First(snapshot, &process)){
        do{
            // Compare process.szExeFile based on format of name, i.e., trim file path
            // trim .exe if necessary, etc.
            if (stricmp(process.szExeFile,name) == 0){
                DWORD pid = process.th32ProcessID;
                result.push_back(OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid));
            }
        } while (Process32Next(snapshot, &process));
    }

    CloseHandle(snapshot);
    return result;
}

bool IsTargetMutex(PCWSTR mutex_name) {
    const std::vector<const wchar_t*> target_names = {
        //WG
        L"wot_client_mutex",
        L"wgc_game_mtx_",
        L"wgc_running_games_mtx",
        //LESTA
        L"lgc_game_mtx_",
        L"lgc_running_games_mtx",
    };

    for (const auto* target_name : target_names) {
        if (wcsstr(mutex_name, target_name)) {
            return true;
        }
    }
    
    return false;
}



//
// Public
//

BOOL SetDebugToken() {
    HANDLE hToken;
    TOKEN_PRIVILEGES tkp;

    /* Setting debug privileges. */
    if (OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
    {
        LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &tkp.Privileges[0].Luid);
        tkp.PrivilegeCount = 1;
        tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
        AdjustTokenPrivileges(hToken, 0, &tkp, sizeof(tkp), NULL, NULL);
    }

    return TRUE;
}

BOOL AllowMultiWot(LPCSTR str)
{
	_NtQuerySystemInformation NtQuerySystemInformation = reinterpret_cast<_NtQuerySystemInformation>(GetLibraryProcAddress("ntdll.dll", "NtQuerySystemInformation"));
	_NtDuplicateObject NtDuplicateObject = reinterpret_cast<_NtDuplicateObject>(GetLibraryProcAddress("ntdll.dll", "NtDuplicateObject"));
	_NtQueryObject NtQueryObject = reinterpret_cast<_NtQueryObject>(GetLibraryProcAddress("ntdll.dll", "NtQueryObject"));

    BOOL flag = FALSE;

    auto processHandles = GetProcessByName(str);
    if (processHandles.empty()) {
        return FALSE;
    }

    for(HANDLE processHandle : processHandles)  {
        NTSTATUS status;
        ULONG handleInfoSize = 0x10000;
        

        PSYSTEM_HANDLE_INFORMATION handleInfo = (PSYSTEM_HANDLE_INFORMATION)malloc(handleInfoSize);

	    /* NtQuerySystemInformation won't give us the correct buffer size, so we guess by doubling the buffer size. */
	    while ((status = NtQuerySystemInformation(SystemHandleInformation, handleInfo, handleInfoSize, NULL)) == STATUS_INFO_LENGTH_MISMATCH)
	    {
		    handleInfo = (PSYSTEM_HANDLE_INFORMATION)realloc(handleInfo, handleInfoSize *= 2);
	    }

	    /* NtQuerySystemInformation stopped giving us STATUS_INFO_LENGTH_MISMATCH. */
	    if (!NT_SUCCESS(status))
	    {
		    return FALSE;
	    }

        for (ULONG i = 0; i < handleInfo->HandleCount; i++)
        {
            SYSTEM_HANDLE handle = handleInfo->Handles[i];
            HANDLE dupHandle = NULL;
            POBJECT_TYPE_INFORMATION objectTypeInfo = nullptr;
            PVOID objectNameInfo = nullptr;
            UNICODE_STRING objectName;
            ULONG returnLength;

            /* Duplicate the handle so we can query it. */
            if (!NT_SUCCESS(NtDuplicateObject(processHandle, (HANDLE)handle.Handle, GetCurrentProcess(), &dupHandle, 0, 0, 0))) {
                continue;
            }

            /* Query the object type. */
            objectTypeInfo = (POBJECT_TYPE_INFORMATION)malloc(0x1000);
            if (!NT_SUCCESS(NtQueryObject(dupHandle, ObjectTypeInformation, objectTypeInfo, 0x1000, NULL))){
                CloseHandle(dupHandle);
                continue;
            }

            /* Query the object name (unless it has an access of 0x0012019f, on which NtQueryObject could hang. */
            if (handle.GrantedAccess == 0x0012019f){
                free(objectTypeInfo);
                CloseHandle(dupHandle);
                continue;
            }

            /* Fix crash on Wine */
            if (objectTypeInfo->Name.Buffer == NULL){
                free(objectTypeInfo);
                CloseHandle(dupHandle);
                continue;
            }

            if (wcscmp(objectTypeInfo->Name.Buffer, L"Mutant") == 0){
                objectNameInfo = malloc(0x1000);
                if (!NT_SUCCESS(NtQueryObject(dupHandle, ObjectNameInformation, objectNameInfo, 0x1000, &returnLength)))
                {
                    /* Reallocate the buffer and try again. */
                    objectNameInfo = realloc(objectNameInfo, returnLength);
                    if (!NT_SUCCESS(NtQueryObject(dupHandle, ObjectNameInformation, objectNameInfo, returnLength, NULL))){
                        free(objectTypeInfo);
                        free(objectNameInfo);
                        CloseHandle(dupHandle);
                        continue;
                    }
                }

                /* Cast our buffer into an UNICODE_STRING. */
                objectName = *(PUNICODE_STRING)objectNameInfo;
                if (objectName.Length)
                {
                    if (IsTargetMutex(objectName.Buffer)){
                        DuplicateHandle(processHandle, (HANDLE)handle.Handle, NULL, NULL, 0, FALSE, 0x1);
                        //CloseHandle(handle.Handle);					
                        flag = TRUE;
                    }
                }
                free(objectNameInfo);
            }
            free(objectTypeInfo);
            CloseHandle(dupHandle);
        }

        free(handleInfo);
        CloseHandle(processHandle);
    }

    return flag;
}
